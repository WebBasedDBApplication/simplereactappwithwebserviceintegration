## Sample React App with Flux

### Description
A simple React App created with ````create-react-app```` and [Flux Architecture](https://facebook.github.io/flux/).

### Usage
Install App via `````yarn install`````, then start with ```yarn start```

### Notice
This example does interact with our webservice, you should have it up and running
