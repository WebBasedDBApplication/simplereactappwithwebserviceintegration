import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

    renderAnimals(){
        let animals = [];
        this.props.sampleStore.animals.forEach((animal) => {
            animals.push(
                // React will complain about uniqueness if we don't have an identifier when we loop elements
                <div key={animal.id}>
                    <h3>{animal.name}</h3>
                    <p>{animal.color}</p>
                </div>
            );
        });
        return animals;
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>
                        Click to load data from your webservice!
                    </p>
                    <button onClick={() => this.props.loadData()}>Click me</button>
                    {(this.props.sampleStore.animals.length !== 0) ? (
                        this.renderAnimals()
                    ) : null}
                </header>
            </div>
        );
    }
}

export default App;
