import React, { Component } from 'react';
import { Container } from 'flux/utils';
import SampleStore from '../store/SampleStore';
import { Actions } from '../actions/ButtonAction';
import App from '../App';

class SampleContainer extends Component {

    static getStores(){
        return [
            SampleStore
        ];
    }

    static calculateState() {
        return {
            sampleStore: SampleStore.getState(),

            loadData: Actions.loadData
        }
    }

    render(){
        return (
            <App {...this.state} />
        );
    }

}

export default Container.create(SampleContainer);

