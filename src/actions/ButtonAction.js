import SampleDispatcher from '../dispatcher/SampleDispatcher';
import axios from 'axios';

export const CLICK_ACTION = "CLICK_ACTION";

export const Actions = {

    loadData(){
        axios.get("http://localhost:8080/api/cat/all").then((response) => {

            // we pass our recieved data to our store
            SampleDispatcher.dispatch({
                type: CLICK_ACTION,
                data: response.data
            })
        }).catch((error)=>{
            console.log(error);
        })
    }
};
