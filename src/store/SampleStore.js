import { ReduceStore } from 'flux/utils';
import SampleDispatcher from '../dispatcher/SampleDispatcher';
import { CLICK_ACTION } from '../actions/ButtonAction';

class SampleStore extends ReduceStore {

    constructor(){
        super(SampleDispatcher)
    }

    getInitialState(){
        return {
            animals: []
        };
    }

    reduce(state, action){
        switch (action.type){
            case CLICK_ACTION:
                state.animals = action.data;
                return {...state};
            default:
                return state;
        }
    }
}

export default new SampleStore();
